//
//  CategoryCell.swift
//  corder-swag
//
//  Created by Trey Earnest on 8/3/17.
//  Copyright © 2017 Trey Earnest. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {

    // cretaed IBOutlet for resuse
    @IBOutlet weak var categoryImage: UIImageView!
    // created IBOutlet for reuse
    @IBOutlet weak var categoryTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
